<?php
use yii\web\AssetBundle;
class MaterialAsset extends AssetBundle{
    public  $sourcePath = '@vendor/aekzaa/yii2-theme-material/assets';
    public  $baseUrl = '@web';
    public  $css=[
        'css/material-wfont.min.css',
        'css/material.min.css',
        'css/ripples.min.css',
        
    ];
    public  $js=[
      'js/material.min.js',
        'js/ripples.min.js',
        
    ];
    public $depents =[
      'yii\webb\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public function init(){
        parent::init();
    }
    
}

