Yii2 Material Design and Bootstrap Theme
========================================
Them for Yii2 Web Application

Installation

php composer.phar require --prefer-dist aekzaa/yii2-theme-material "*"


or Add

"aekzaa/yii2-theme-material": "*"

to the require section of your composer.json file.

Usage 
-------
open your layout views/layouts/main.php and add

use aekzaa\theme\material;

material\MaterialAsset::register($this);